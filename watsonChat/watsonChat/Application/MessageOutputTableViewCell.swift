//
//  MessageOutputTableViewCell.swift
//  watsonChat
//
//  Created by anderson.jesus.silva on 05/04/19.
//  Copyright © 2019 anderson.jesus.silva. All rights reserved.
//

import UIKit

class MessageOutputTableViewCell: UITableViewCell {

    @IBOutlet weak var txMessage: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setup(message:MessageOutput) {
        if let text = message.text {
            self.txMessage.text = text
            self.txMessage.textAlignment = .right
            if message.receive == ReceiveText.ASSISTANT {
                self.txMessage.textAlignment = .left
            }            
        }
        
    }

}
