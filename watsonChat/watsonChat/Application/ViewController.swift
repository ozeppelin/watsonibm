//
//  ViewController.swift
//  watsonChat
//
//  Created by anderson.jesus.silva on 04/04/19.
//  Copyright © 2019 anderson.jesus.silva. All rights reserved.
//

import UIKit
import Assistant

let VERSION = "2019-04-04"
let API_KEY = "wCXeQxl0j3FXeJqQv8SAZWue0g3GiSNZb-w5qs4lVRfj"
let API_KEY2 = "lEGAgVVtjL7pSSzg6976xJZEeuWHgBnMEVYlaKsR9GYR"
let URL_DALLAS = "https://gateway.watsonplatform.net/assistant/api"
let WORKSPACE_ID = "42107835-39f4-4451-b2b4-fcb048a9ad81"
let ASSISTENT_ID = "1c128501-fbe1-4d6c-9c7d-1445e0fc7061"
let SKILL_ID = "10cd0de2-b3ab-4c9a-bce1-54ec2c57524f"
let USER_NAME = "apikey"
let PASSWORD  = "Devc841004!2"

struct MessageOutput {
    var type:String
    var text:String?
    var receive:ReceiveText
    
    init(type:String, text:String?, receive:ReceiveText) {
        self.type = type
        self.text = text
        self.receive = receive
    }
}

enum ReceiveText {
    case ME, ASSISTANT
}

class ViewController: UIViewController {

    let background = DispatchQueue.global()
    var assistent:Assistant?
    var sessionID:String?
    var messageOutput:[MessageOutput] = [MessageOutput]()
    var reverse:[MessageOutput]!
    var total = 0
    var messageUser:String = ""
    let dispatchGroup = DispatchGroup()
    
    @IBOutlet weak var tbView: UITableView!
    @IBOutlet weak var txBox: UITextView!
    @IBOutlet weak var btSend: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.tbView.transform = CGAffineTransform(scaleX: 1, y: -1)
        self.tbView.tableFooterView = UIView()
        connectAssistent()
    }

    @IBAction func sendText(_ sender: Any) {
        self.validateMessages()
    }
    
    private func connectAssistent() {
        
        self.assistent = Assistant(version: VERSION, apiKey: API_KEY2)
        self.assistent!.serviceURL = URL_DALLAS
        background.async {
            self.createSession(assistant: self.assistent!)
        }
    }
    
    private func validateMessages() {
        if !self.txBox.text.isEmpty {
            self.messageUser = self.txBox.text
            let messageInput = MessageInput(messageType: "text", text: self.messageUser)
            self.txBox.text = ""
            
            self.sendMessageToTable(outputMessage: MessageOutput(type: "text", text: self.messageUser, receive: .ME))
            
            if let sessionID = self.sessionID {
                let backgroud = DispatchQueue.global()
                backgroud.async {
                    self.sendMessage(session: sessionID, messageInput: messageInput)
                }
            }
        }
    }
    
    private func sendMessageToTable(outputMessage:MessageOutput) {
        self.messageOutput.append(outputMessage)
        self.reverse = self.messageOutput.reversed()
        self.tbView.reloadData()
    }
    
    private func createSession(assistant:Assistant) {
        
        assistant.createSession(assistantID: ASSISTENT_ID) { (response, error) in
            
            guard let session = response?.result else {
                print("ERROR: \(error?.localizedDescription ?? "ERROR DESCONHECIDO!")")
                return
            }
            
            self.sessionID = session.sessionID
        }
        
    }
    
    private func sendMessage(session:String, messageInput:MessageInput) {
        
        if let assistant = self.assistent {
            
            assistant.message(assistantID: ASSISTENT_ID, sessionID: session, input: messageInput, context: nil, headers: nil) { (response, error) in
                guard let messageResult = response?.result else {
                    print("ERROR: \(error?.localizedDescription)")
                    return
                }
                
                if let generic = messageResult.output.generic {
                    let type = generic.map({ $0.responseType })
                    let textOutput = generic.filter({ (result) -> Bool in
                        if let text = result.text, !text.isEmpty {
                            return true
                        }
                        return false
                    }).map({ $0.text })
                    
                    
                    if textOutput.count > 0 && type.count > 0 {
                        self.sendMessageToTable(outputMessage: MessageOutput(type: type[0], text: textOutput[0], receive: .ASSISTANT))
                    }
                }
            }
            
        }
        
    }
    
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.messageOutput.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tbView.dequeueReusableCell(withIdentifier: "cellResult", for: indexPath) as! MessageOutputTableViewCell
        cell.setup(message: self.reverse[indexPath.row])
        cell.contentView.transform = CGAffineTransform(scaleX: 1, y: -1)
        return cell
        
    }
    
}
